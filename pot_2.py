from gpiozero import MCP3008
from time import sleep

adc0 = MCP3008(channel=0)
adc1 = MCP3008(channel=1)
adc2 = MCP3008(channel=2)
adc3 = MCP3008(channel=3)
while True:
    pot0 = round(adc0.value*120.0,1)
    pot1 = round(adc1.value*120.0,1)
    pot2 = round(adc2.value*120.0,1)
    pot3 = round(adc3.value*120.0,1)
    print("POT0:", pot0,"\tPOT1:", pot1,"\tPOT2:", pot2,"\tPOT3:", pot3)
    sleep(1)
