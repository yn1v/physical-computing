from gpiozero import PWMLED
from signal import pause

red = PWMLED(17)
blue = PWMLED(22)
green = PWMLED(27)
red.pulse()
blue.pulse()
pause()
