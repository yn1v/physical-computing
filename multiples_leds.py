from gpiozero import LED
from time import sleep

rojo = LED(17)
verde = LED(27)
azul = LED(22)
rojo.blink()
sleep(0.5)
verde.blink()
sleep(0.33)
#azul.blink()
azul.blink(on_time=0.33, off_time=0.25)
