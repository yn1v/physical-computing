from gpiozero import MCP3008, AngularServo
from time import sleep


servo0 = AngularServo(17, min_angle=0, max_angle=120)
servo1 = AngularServo(27, min_angle=0, max_angle=120)
servo2 = AngularServo(22, min_angle=0, max_angle=120)
servo3 = AngularServo(23, min_angle=0, max_angle=90)

pausa = 2

adc0 = MCP3008(channel=0)
adc1 = MCP3008(channel=1)
adc2 = MCP3008(channel=2)
adc3 = MCP3008(channel=3)
while True:
#    pot0 = int(round(adc0.value*120.0,1))
 #   pot1 = int(round(adc1.value*120.0,1))
  #  pot2 = int(round(adc2.value*120.0,1))
  #  pot3 = int(round(adc3.value*90.0,1))
   # print("POT0:", pot0,"\tPOT1:", pot1,"\tPOT2:", pot2,"\tPOT3:", pot3)
    
    servo0.angle = 45
    servo1.angle = 45 
    servo2.angle = 45
    servo3.angle = 45
    sleep(pausa)
    servo0.angle = 0
    servo1.angle = 0
    servo2.angle = 120
    servo3.angle = 86
    sleep(pausa)
    servo0.angle = 0
    servo1.angle = 0 
    servo2.angle = 120
    servo3.angle = 41
    sleep(pausa)
    servo0.angle = 0
    servo1.angle = 0 
    servo2.angle = 51
    servo3.angle = 45
    sleep(pausa)
    servo0.angle = 120
    servo1.angle = 0 
    servo2.angle = 51
    servo3.angle = 45
    sleep(pausa)
    servo0.angle = 0
    servo1.angle = 0 
    servo2.angle = 51
    servo3.angle = 45
    sleep(pausa)
    servo0.angle = 120
    servo1.angle = 0 
    servo2.angle = 120
    servo3.angle = 45
    sleep(pausa)
    servo0.angle = 120
    servo1.angle = 0 
    servo2.angle = 120
    servo3.angle = 90
    sleep(pausa)
