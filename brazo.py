from gpiozero import MCP3008, AngularServo
from time import sleep


servo0 = AngularServo(17, min_angle=0, max_angle=120)
servo1 = AngularServo(27, min_angle=0, max_angle=120)
servo2 = AngularServo(22, min_angle=0, max_angle=120)
servo3 = AngularServo(23, min_angle=0, max_angle=90)

pausa = 0.5

adc0 = MCP3008(channel=0)
adc1 = MCP3008(channel=1)
adc2 = MCP3008(channel=2)
adc3 = MCP3008(channel=3)
while True:
    pot0 = int(round(adc0.value*120.0,1))
    pot1 = int(round(adc1.value*120.0,1))
    pot2 = int(round(adc2.value*120.0,1))
    pot3 = int(round(adc3.value*90.0,1))
    print("POT0:", pot0,"\tPOT1:", pot1,"\tPOT2:", pot2,"\tPOT3:", pot3)
    servo0.angle = pot0
    servo1.angle = pot1 
    servo2.angle = pot2
    servo3.angle = pot3
    sleep(pausa)
