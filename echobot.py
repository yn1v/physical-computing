#!/usr/bin/env python
# -*- coding: utf-8 -*-
import glob
import logging
import telegram
from telegram.error import NetworkError, Unauthorized
from time import sleep
from gpiozero import LED

rojo = LED(17)

base_dir = '/sys/bus/w1/devices/'
device_folder = glob.glob(base_dir + '28*')[0]
device_file = device_folder + '/w1_slave'

def read_temp_raw():
    f = open(device_file, 'r')
    lines = f.readlines()
    f.close()
    return lines

def read_temp():
    lines = read_temp_raw()
    while lines[0].strip()[-3:] != 'YES':
        sleep(0.2)
        lines = read_temp_raw()
    equals_pos = lines[1].find('t=')
    if equals_pos != -1:
        temp_string = lines[1][equals_pos+2:]
        temp_c = float(temp_string) / 1000.0
        # temp_f = temp_c * 9.0 / 5.0 + 32.0
        return temp_c #, temp_f

#while True:
#    print(read_temp())
#    sleep(1)



"""Simple Bot to reply to Telegram messages.

This is built on the API wrapper, see echobot2.py to see the same example built
on the telegram.ext bot framework.
This program is dedicated to the public domain under the CC0 license.
"""


update_id = None


def main():
    """Run the bot."""
    global update_id
    # Telegram Bot Authorization Token
    bot = telegram.Bot('816284517:AAFbjd4c2Ob8Jk40WcKq06sQDnNYZsatb2E')

    # get the first pending update_id, this is so we can skip over it in case
    # we get an "Unauthorized" exception.
    try:
        update_id = bot.get_updates()[0].update_id
    except IndexError:
        update_id = None

    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    while True:
        try:
            echo(bot)
        except NetworkError:
            sleep(1)
        except Unauthorized:
            # The user has removed or blocked the bot.
            update_id += 1


def echo(bot):
    """Echo the message the user sent."""
    global update_id
    # Request updates after the last update_id
    for update in bot.get_updates(offset=update_id, timeout=10):
        update_id = update.update_id + 1

        texto = update.message.text.lower()
        valor = read_temp()
        
        if texto == "hola":  # your bot can receive updates without messages
            # Reply to the message
            update.message.reply_text("Como estas?")
        elif texto == "bye":
            update.message.reply_text("Hasta Luego")
        elif texto == "on":
            rojo.on()
        elif texto == "off":
            rojo.off()
        elif texto == "blink":
            rojo.blink(on_time=.25, off_time=.15)
        elif texto == "temp":
                       
            if valor > 30:
                update.message.reply_text(str(valor) + "\nLa pucha, nos estamos asando!")
            elif valor > 28:
                update.message.reply_text(str(valor) + "\nHace Calor")
        else:
            update.message.reply_text("Qué?")


if __name__ == '__main__':
    main()
