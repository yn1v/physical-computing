Esquema general

Primera Semana

Configuración de las herramientas de trabajo. Introducción a Python.

Breadboard y electrónica básica. Encender un led.

PWM RGB. Voltaje, Corriente y tierra comun. Servos.

Conectar un botón. Obtener datos de sensores digitales. Condicionales.

Segunda Semana

Cosntruir un chat bot básico

Ampliar las funciones del chat bot

Implementar acciones con el chat bot. Leer datos del GPIO, ejecutar acciones.

Pulir las funcionalidades del chat bot.
