
Introducción a Python:

    1. Print y “Hola Mundo!”
    2. Operaciones básicas
        a) Suma, resta, multiplicación y división
        b) Módulo (cociente entero) y Remanente (resto) 
        c) Potencia
    3. Variables y tipos
        a) imprimir variables ( “+” y “,” )
    4. Listas
        a) Operaciones de Listas
    5. Bucles “For”
        a) range
        b) iterables
    6. Boleanos y Condicionales
        a) if
        b) else
        c) elif
        d) in
    7. Bucles While
    8. Funciones
    9. Manipulación de Texto
