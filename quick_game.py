# Importar librerias a usar
from time import sleep, time

# hacer una pausa
sleep(5)

# anotar el tiempo de inicio
inicio = time()

# Enviar aviso a pantalla
print ('Rápido, Presiona la tecla "Enter"')

# Usar funcion input para capturar la tecla oprimida
input()

# Calcular el tiempo que tomó. Tiempo actual menos el tiempo de inicio
tiempo_de_reaccion = time() - inicio

# Imprimir resultados

print('Eso fue rápido!')
print('Te tomó', tiempo_de_reaccion, 'segundos')

# Reto:
# Se podría cambiar el tiempo de espera por un valor aleatorio?
#
# tomado de https://docs.python.org/2/library/random.html
# Return a random integer N such that a <= N <= b.
# ejemplo de sintaxis: random.randint(a, b)
# Hay que importar la libreria random
