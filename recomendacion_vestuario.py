# Esta es una lista de posibles respuestas
clima = ["lluvioso", "soleado", "caluroso", "agradable", "frio", "ventoso"]

respuesta = input("Como esta el clima?")
# por el momento no vamos a chequear si las respuestas llevan mayusculas o acentos
# asi que por mientras usemos solo minusculas sin acentos

if respuesta in clima:
    print("Deberias vestir chinelas") # Borrar o comentar esta linea
    # El primer reto es asignar una prenda de vestir que coincida con clima
    # Ejemplo: Si esta lluvioso, sugerir que lleve un paraguas

else:
    # si lo que escribe la persona no esta en la lista brindará esta respuesta
    print("Eso es una novedad. Que tengas un bonito día!")



