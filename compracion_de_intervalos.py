
# Usando una variable intermedia para convertir a enteros
variable_temporal = input("Cual es tu altura?")
altura = int(variable_temporal)


# forma explicita de comparación
if altura >= 110 and altura <= 190:
    print ("Tu altura esta en el rango adecuado")
else:
    print ("Tu altura esta fuera del aceptado")

# Convirtiendo a enteros de forma directa
peso = int(input("Cual es tu peso?"))

# forma abreviada de compración
if 100 <= peso <= 300:
    print("Tu peso esta en el rango adecuado")

else:
    print("Tu peso esta fuera del rango aceptado")


