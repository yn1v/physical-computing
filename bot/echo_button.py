#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Simple Bot to reply to Telegram messages.

This is built on the API wrapper, see echobot2.py to see the same example built
on the telegram.ext bot framework.
This program is dedicated to the public domain under the CC0 license.
"""
import logging
import telegram
from telegram.error import NetworkError, Unauthorized
from time import sleep
from gpiozero import Button

boton = Button(17)

update_id = None


def main():
    """Run the bot."""
    global update_id
    # Telegram Bot Authorization Token
    bot = telegram.Bot('816284517:AAFbjd4c2Ob8Jk40WcKq06sQDnNYZsatb2E')

    # get the first pending update_id, this is so we can skip over it in case
    # we get an "Unauthorized" exception.
    try:
        update_id = bot.get_updates()[0].update_id
    except IndexError:
        update_id = None

    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    while True:
        try:
            echo(bot)
        except NetworkError:
            sleep(1)
        except Unauthorized:
            # The user has removed or blocked the bot.
            update_id += 1


def echo(bot):
    """Echo the message the user sent."""
    global update_id
    # Request updates after the last update_id
    for update in bot.get_updates(offset=update_id, timeout=10):
        update_id = update.update_id + 1
        texto = update.message.text.lower()
        nombre = update.message.chat.first_name
        conector = " dijo "
        mensaje_de_salida = ()
        print(texto)

        if texto == "hola":  # your bot can receive updates without messages
            # Reply to the message
            update.message.reply_text("Como estas?")
        elif texto == "bye":
            update.message.reply_text("Hasta Luego")
        elif texto == "alerta":
            update.message.reply_text("Ahora monitoreando actividades")
            contador = 40
            while contador != 0:
                if boton.is_pressed:
                    print("se activó el circuito")
                    update.message.reply_text("AAAAHHHGGG")
                sleep(0.25)
                contador -= 1
        else:
            update.message.reply_text("Qué?")


if __name__ == '__main__':
    main()
