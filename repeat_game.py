from time import sleep, time
total_time = 0
games = 1
keep_playing = True

while keep_playing:
    print('Get ready')
    sleep(5)
    start = time()
    print('Quick, press the Enter key')
    input()
    reaction_time = time() - start
    if reaction_time < 1:
        print('That was fast')
    print('You took', reaction_time, 'seconds')
    total_time = total_time + reaction_time
    if games != 1:
        print('You have played:', games, 'times')
        print('You average times is:', total_time/games)
    print()
    print("Want to play again?")
    print("Press 'y' play again or any other key to exit the game")
    more = input('? ')
    if more.lower() == 'y' or more.lower() == 'yes':
        games +=1
    else:
        keep_playing = False





