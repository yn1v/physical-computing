from time import sleep, time
answer = []
total = 0
tries = 3
for i in range(tries):
    print("Get ready to press 'Enter'")
    sleep(3)
    start = time()
    print("Press 'Enter' now!")
    input()
    answer.append(time() - start)
    print("You took", answer[i], "seconds")
    print("")
for a in answer:
    total += a
print("You average time was:", total/tries, "seconds") 


